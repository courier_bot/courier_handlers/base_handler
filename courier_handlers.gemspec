# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'courier_handlers/base_handlers/version'

Gem::Specification.new do |spec|
  spec.name          = 'courier_handlers'
  spec.version       = CourierHandlers::BaseHandlers::VERSION
  spec.authors       = 'Zyrthofar'
  spec.email         = 'zyrthofar@protonmail.com'

  spec.summary       = 'Base class for courier handlers'
  spec.description   = 'Base class for all delivery handlers used by courier_handlers.'
  spec.homepage      = 'https://gitlab.com/courier_bot/courier_handlers/base_handler'
  spec.license       = 'MIT'

  spec.files = %w[
    lib/courier_handlers.rb
    lib/courier_handlers/base_handler.rb
    lib/courier_handlers/callback_ignore_request.rb
    lib/courier_handlers/callback_request.rb
    lib/courier_handlers/contact_info_delivery_end_response.rb
    lib/courier_handlers/contact_info_response.rb
    lib/courier_handlers/delivery_request.rb
    lib/courier_handlers/base_handlers/version.rb
  ]

  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
end
