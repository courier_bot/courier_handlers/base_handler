# frozen_string_literal: true

RSpec.describe CourierHandlers::BaseHandler do
  describe '.deliver' do
    subject { CourierHandlers::BaseHandler.new.deliver(field: 'value') }

    it 'raises a `NotImplementedError`' do
      expect { subject }.to raise_error(NotImplementedError)
    end
  end

  describe '.callback' do
    subject { CourierHandlers::BaseHandler.callback({}) }

    it 'raises a `NotImplementedError`' do
      expect { subject }.to raise_error(NotImplementedError)
    end
  end
end
