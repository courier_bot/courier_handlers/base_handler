# frozen_string_literal: true

module CourierHandlers
  # Special case of a contact info response when the client does not have any more delivery methods
  # to try for the current message. The server should stop trying to deliver the message.
  class ContactInfoDeliveryEndResponse < ContactInfoResponse
    # If true, the client has asked to stop delivering the message.
    def end_of_delivery?
      true
    end
  end
end
