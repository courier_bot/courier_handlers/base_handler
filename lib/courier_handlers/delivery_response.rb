# frozen_string_literal: true

module CourierHandlers
  # Normalization of a third-party service's response after an initial delivery.
  #
  # The attributes are the following:
  # * `success`: Whether the third-party service has accepted to deliver the message. This does not
  #   mean that the message was successfully delivered.
  # * `external_id`: The third-party service's id for the delivery.
  DeliveryResponse = Struct.new(:success, :external_id)
end
