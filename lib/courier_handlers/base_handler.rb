# frozen_string_literal: true

module CourierHandlers
  # Base handler class that all handlers should inherit from.
  class BaseHandler
    # Makes a request to a third-party service's API to deliver a message.
    #
    # The fields should be the ones given by the client, as per a handler's requirements. They will
    # be passed through the handler's normalizer. Each handler may have different required fields,
    # and these can be verified in the respective handlers.
    #
    # @params fields [Hash] The delivery fields, so that the request to the third-party service's
    #   API can be made.
    # @return [CourierHandlers::DeliveryResponse] The normalized delivery response.
    def deliver(fields)
      fields = normalize_delivery_fields(fields)
      _deliver(fields)
    end

    # After a third-party service has accepted to deliver a message, it may make a callback request
    # to to the server to give updates about the delivery. That request is given to the proper
    # handler to normalize the request.
    #
    # If the return value is a `CourierHandlers::CallbackIgnoreRequest` instance, it means that the
    # server should ignore the callback.
    #
    # @params args [Hash] The third-party service's callback body.
    # @return [CourierHandlers::CallbackRequest] The normalized callback request.
    def self.callback(_args)
      raise NotImplementedError
    end

    protected

    # Normalizer method for delivery fields.
    def normalize_delivery_fields(fields)
      fields
    end

    # Internal implementation of the request to the third-party service's API.
    def _deliver(_fields)
      raise NotImplementedError
    end
  end
end
