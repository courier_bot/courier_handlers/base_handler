# frozen_string_literal: true

module CourierHandlers
  # Normalization of a third-party service's request after it has news about a delivery.
  #
  # The attributes are the following:
  # * `success`: Whether the third-party service has successfully delivered the message.
  # * `external_id`: The third-party service's id for the delivery.
  # * `error`: The reason why the delivery failed, if any.
  CallbackRequest = Struct.new(:success, :external_id, :error) do
    # If true, the server should ignore the callback request.
    def ignore?
      false
    end
  end
end
