# frozen_string_literal: true

module CourierHandlers
  # Special case of a third-party service's callback request when the server does not care about
  # what happened. The server should ignore the callback.
  class CallbackIgnoreRequest < CallbackRequest
    # If true, the server should ignore the callback request.
    def ignore?
      true
    end
  end
end
