# frozen_string_literal: true

module CourierHandlers
  # Normalization of a client's response following a request for contact information when trying
  # to deliver a message.
  #
  # The attributes are the following:
  # * `success`: Whether the client has successfully responded to the request for contact
  #   information.
  # * `contact_kind`: How the client wants the message to be delivered.
  # * `fields`: Arbitrary `Hash` object of fields required to make a delivery request to the related
  #   third-party service.
  ContactInfoResponse = Struct.new(:success, :contact_kind, :fields) do
    # If true, the client has asked to stop delivering the message.
    def end_of_delivery?
      false
    end
  end
end
